package we.consumingrest_task.repository;

import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import we.consumingrest_task.model.HouseTargaryenTitles;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@Component
public class TargaryenRepository {

    public String houseTargaryenTitles(int id) throws  IOException{
        URL url = new URL("https://anapioficeandfire.com/api/houses/" + id);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
            InputStreamReader reader = new InputStreamReader(connection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;
            StringBuffer responseContent = new StringBuffer();

            while ((line = bufferedReader.readLine()) != null){
                responseContent.append(line);
            }
            bufferedReader.close();

            JSONObject targaryenObject = new JSONObject(responseContent.toString());
            var titlesList = targaryenObject.getJSONArray("titles").toString(4);

            HouseTargaryenTitles titles = new HouseTargaryenTitles(titlesList);
            return titles.getTitles();
        }
        return null;
    }
}
