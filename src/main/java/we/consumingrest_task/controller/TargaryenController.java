package we.consumingrest_task.controller;

import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import we.consumingrest_task.model.HouseTargaryenTitles;
import we.consumingrest_task.repository.TargaryenRepository;

import java.util.Arrays;

@RestController
@RequestMapping("api/v1/houseTargaryen/titles")
public class TargaryenController {

    private final TargaryenRepository targaryenRepository;

    @Autowired
    public TargaryenController(TargaryenRepository targaryenRepository) {
        this.targaryenRepository = targaryenRepository;
    }

    @GetMapping("/{id}")
    public ResponseEntity<String> houseTargaryenTitles(@PathVariable int id) {

        try {
            return new ResponseEntity<>(targaryenRepository.houseTargaryenTitles(id), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
}